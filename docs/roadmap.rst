Roadmap
********
.. _target to roadmap:

Planned activities:

1. Including OSM's internal Time Series DB (Prometheus) as a monitoring source.
2. Support for model training and collection of data.
3. Support for AI Models in which the training and production stages are not clearly separated (such as reinforcement).
