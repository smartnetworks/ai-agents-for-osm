Usage License
***************

OSM AI Agent is an Open Source Project maintained by ATOS SE, its usage and distribution shall be restricted to those
defined in **Apache 2.0 License**.


.. include:: ../LICENSE
   :literal:
