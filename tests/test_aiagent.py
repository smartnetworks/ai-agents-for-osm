import json
from pathlib import Path
from unittest import mock

from aiagent.agent import AIAgent


def _prepare_agent():
    with open(Path(__file__).parent / Path('resources', 'operation_status_ok.json')) as status:
        operation_status_ok_d = json.loads(status.read())

    agent = AIAgent()
    agent.osm_interface = mock.MagicMock()
    agent.osm_interface.scale_ns.return_value = {'id': 'some-id'}
    agent.osm_interface.get_operation_state_scale.return_value = operation_status_ok_d
    return agent


def test_request_scaling_happens():
    with open(Path(__file__).parent / Path('resources', 'expected_operational_data.json')) as payload:
        operational_data = json.loads(payload.read())
    agent = _prepare_agent()
    assert agent.request_scale_action(operational_data, True)
    assert agent.osm_interface.scale_ns.called


def test_request_scaling_do_not_happen():
    with open(Path(__file__).parent / Path('resources', 'expected_operational_data.json')) as payload:
        operational_data = json.loads(payload.read())
    agent = _prepare_agent()
    assert not agent.request_scale_action(operational_data, False)  # min-instance reached
    assert not agent.osm_interface.scale_ns.called
