import click

from sns_logger import logger

from aiagent.agent import AIAgent

log = logger.get_logger(__name__)


@click.group()
def aiagent():
    pass


# TODO: Replace cronjob with schedule library
@aiagent.command(name='start')
def start():
    agent = AIAgent()
    agent.run()
