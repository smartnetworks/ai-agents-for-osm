Contribute to the code
======================

Report a Bug
--------------

.. _target to report_a_bug:

Feel free to open issues in `Gitlab <https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/issues>`_

Contribution Guidelines
------------------------

This repo follows a Gitflow methodology. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or MR created from GiLab.
3. Commit and push frequently.
4. When ready, open a Merge request to develop and wait for approval.
5. Only hotfixes and planned releases will be merged into Master and a new docker image will be made available.

Thanks a lot for contributing!
