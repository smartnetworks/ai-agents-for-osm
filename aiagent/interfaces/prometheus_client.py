import time
import requests
from datetime import datetime, timedelta

from sns_logger import logger

from aiagent import utils

log = logger.get_logger(__name__)

# TODO: This shall be used as source of monitoring data when a CPU forecast Model is available
#  as well as to train models


class PrometheusClient:
    def __init__(self, url="http://localhost", port=9090):
        self.prometheus_url = f"{utils.url_composer(url, port)}/api/v1/query_range"
        log.info(f"Prometheus client created with url {self.prometheus_url}")

    def range_query(self, metric, instance=None, step=60, days=1):
        """
        Returns a PrometheusData object loaded with results from a query_range call

        Args:
            metric: string of the metric query
            instance: instance to query
            step: the step size
            days: Number of days to query

        Returns: PrometheusData
        """
        params = {
            'query': metric,
            'instance': instance,
            'step': step,
            'start': self._time_to_epoch((datetime.now() - timedelta(days=days))),
            'end': self._time_to_epoch(datetime.now())
        }
        payload = self._request_past_data(params)
        data = self._extract_data(payload)
        return data

    def _request_past_data(self, params):
        response = requests.get(self.prometheus_url, params)
        response.raise_for_status()
        return response.json()

    @staticmethod
    def _time_to_epoch(t):
        """
        A simple utility to turn a datetime object into a timestamp
        """
        return int(time.mktime(t.timetuple()))

    @staticmethod
    def _extract_data(payload):
        return [] if payload['status'] != 'success' else payload['data']['result']

    @staticmethod
    def extract_values(ts_data):
        """
        Post processing of the obtained metrics from Prometheus

        Args:
            ts_data (list):

        Returns (dict): dict in the Tensorflow required format

        """
        # TODO: This post-processing should be configured in values.yml
        try:
            ts_tuple_l = ts_data[0]['values']
        except Exception as e:
            log.warning(f"No data in Prometheus values ts: {ts_data}")
            ts_tuple_l = []
        return {'instances': ts_tuple_l}
