image: registry.atosresearch.eu:18467/cython:3.8-slim-cicd

variables:
  PACKAGE: aiagent
  IMAGE_NAME: osm-ai-agent
  JOB_WITH_COVERAGE: tests
  GITLAB_URL: https://scm.atosresearch.eu
  NEXUS_PYPI_COMMON_URL_DOWNLOAD: https://$NEXUS_USER:$NEXUS_PAT@registry.atosresearch.eu:8443/repository/common-pypi/simple

stages:
  - publish_pages
  - pre
  - build
  - tests
  - qa
  - release

clean_up:
  stage: pre
  tags:
    - shell
  only:
    - develop
    - master
    - merge_requests
  before_script:
    - echo "clean up local CICD Host"
  script:
    - rm -rf reports
    - mkdir -p reports
    - docker rmi -f $(docker images -a | grep '<none>' | awk '{print $3}') || true
    - docker rmi -f $(docker images -a | grep $DOCKERHUB_IMAGE_PREFIX/$IMAGE_NAME | awk '{print $3}') || true
    - docker rm -f -v $(docker ps -a | grep $DOCKERHUB_IMAGE_PREFIX/$IMAGE_NAME | awk '{print $1}') || true

before_script:
  - docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_PAT" $DOCKERHUB_REGISTRY
  - echo "Pulling CI image"
  - docker pull ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci  || true

build_image:
  stage: build
  tags:
    - shell
  only:
    - develop
    - master
    - merge_requests
  script:
    - echo "Preparing local environment"
    - rm -rf reports
    - mkdir -p reports
    - touch reports/version.txt
    - chmod +x cicd/generate_pip_conf.sh
    - sh cicd/generate_pip_conf.sh ${NEXUS_PYPI_COMMON_URL_DOWNLOAD}
    - docker build  --no-cache -t ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci --add-host=registry.atosresearch.eu:192.168.145.8 -f cicd/Dockerfile .
    - rm -f pip.conf
    - echo "Pushing a temporal image to Dockerhub"
    - docker push ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci
    - VERSION=$(docker run --entrypoint python ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci -c "import pkg_resources; print(pkg_resources.get_distribution('${PACKAGE}').version)" )
    - echo ${VERSION} >> reports/version.txt
  after_script:
    - docker rm -f -v  $(docker ps -a | grep ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci | awk '{print $1}') || true
    - docker rm -f -v $(docker ps -a | grep 'python -c' | awk '{print $1}') || true
  artifacts:
    paths:
      - reports/version.txt


tests:
  stage: tests
  tags:
    - shell
  only:
    - develop
    - master
    - merge_requests
  script:
    - docker run -v $(pwd)/reports/qa:/usr/src/app/reports/qa --entrypoint pylint ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci $PACKAGE tests --exit-zero --reports y >> qa_report.txt; mkdir -p reports/qa; sudo chmod 777 -R reports/; sudo chmod 777 qa_report.txt; mv qa_report.txt reports/qa/;
    - docker run -v $(pwd)/reports/htmlcov:/usr/src/app/htmlcov --entrypoint pytest ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci tests --cov=${PACKAGE} --cov-report html --cov-report term-missing
  after_script:
    - docker rm -f -v  $(docker ps -a | grep ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci | awk '{print $1}') || true
    - sudo chmod -R 777 reports/
    - docker rm -f -v $(docker ps -a | grep pylint | awk '{print $1}') || true
    - docker rm -f -v $(docker ps -a | grep pytest | awk '{print $1}') || true
  coverage: '/TOTAL.+?(\d+%)/'
  artifacts:
    paths:
      - reports/htmlcov
      - reports/qa

qa:
  stage: qa
  dependencies:
    - tests
  only:
    - develop
    - master
    - merge_requests
  before_script:
    - echo "Using cython:3.8-slim-cicd docker container"
  script:
    # pylint will fail return non-zero even if only warnings are found
    - pylint_wrapper --pylint-report-path reports/qa/qa_report.txt raise-for-code
    - QA=$(pylint_wrapper --pylint-report-path reports/qa/qa_report.txt get-qa-value | tail -1)
    - echo ${QA}
    - echo ${QA} >> reports/qa/score.txt
  artifacts:
    paths:
      - reports/qa

documentation:
  stage: tests
  before_script:
    - echo "Using cython:3.8-slim-cicd docker container"
  script:
    - pip install -r requirements_docs.txt
    - cd docs
    - make html SPHINXOPTS="-W --keep-going -n"
  only:
  - develop
  - master
  - merge_requests
  artifacts:
    paths:
      - docs/build/html

make_badges: # Only update when merged to main branches
  stage: release
  dependencies:
    - qa
    - build_image
  only:
    - master
    - develop
  before_script:
    - echo "Using cython:3.8-slim-cicd docker container"
  script:
    - QA=$(cat reports/qa/score.txt)
    - VERSION=$(cat reports/version.txt)
    - gitlab_wrapper update-badge --url $GITLAB_URL --api-token ${API_TOKEN}
      --project-id ${CI_PROJECT_ID} --pipeline-id ${CI_PIPELINE_ID} --version ${VERSION} --version-badge-name Version
      --test-job-name $JOB_WITH_COVERAGE --coverage-badge-name Coverage --qa-value ${QA} --pylint-badge-name Pylint
  artifacts:
    paths:
      - Coverage.svg
      - Version.svg
      - Pylint.svg

upload_release:
  stage: release
  tags:
    - shell
  dependencies:
    - build_image
  only:
    - master
  script:
    - VERSION=$(cat reports/version.txt)
    - docker tag ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:gitlab-ci ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:${VERSION}
    - docker tag ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:${VERSION} ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:latest
    - echo "Pushing final image to DockerHub tagged to version"
    - docker push ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:${VERSION}
    - echo "Pushing final image to DockerHub tagged as latest"
    - docker push ${DOCKERHUB_IMAGE_PREFIX}/${IMAGE_NAME}:latest


pages:
  image: python:3.7-alpine
  stage: publish_pages
  before_script:
    - echo "using clean env"
  when: manual
  script:
    - pip install -r requirements_docs.txt
    - pip install -U sphinx
    - sphinx-build -b html docs/ public
  artifacts:
    paths:
    - public
