import requests

from sns_logger.logger import get_logger

log = get_logger(__name__)


class MonitoringInterface:
    def __init__(self, monitoring_d, operational_data):
        """
        Interface to extract monitoring metrics that will be fed to an AI Model

        Args:
            monitoring_d (dict): dictionary from which the endpoint is obtained
            operational_data (dict): Relevant information obtained from OSM

        """
        # TODO: Document this possibility. The VNF is the source of monitoring data
        if monitoring_d['endpoint'] == 'vnf':
            self.endpoint = operational_data['vdu-data']['ip-address']
        # TODO: Allow prometheus Client as source of monitoring data
        else:
            self.endpoint = monitoring_d['endpoint']
        # TODO: Create get_health()

    def get_metrics(self):
        """
        Exposed method to request monitoring metrics from the configured endpoint

        Returns (dict):
            metrics {'instances': metrics_l}
        """
        response = requests.get(self.endpoint)
        response.raise_for_status()
        metrics_d = response.json()
        log.info(f"Metrics Obtained: {str(metrics_d)[:120]}")
        return metrics_d
