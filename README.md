master
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/master/raw/Coverage.svg?job=make_badges)
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/master/raw/Pylint.svg?job=make_badges)
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/master/raw/Version.svg?job=make_badges)

develop
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Coverage.svg?job=make_badges)
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Pylint.svg?job=make_badges)
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Version.svg?job=make_badges)
![Not found](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/badges/develop/pipeline.svg)

[[_TOC_]]

# OSM-AI-Agent

AI-Agent source code to deploy a container in the EE of OSM that performs proactive monitoring and requests scaling actions
over the attached VNF. 

This is an Open Source project maintained by ATOS R&D Smart Networks & Services.
1. Access full documentation in [ReadTheDocs](https://ai-agents-for-osm.readthedocs.io/en/latest/)
2. Pull the latest docker image from [DockerHub](https://hub.docker.com/r/atosresearch/osm-ai-agent)
3. Create issues for features/bugs in [GitLab](https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/)

## How to install

Installation of the AIAgent requires access to the internal ATOS R&D Nexus and VPN. This is granted to all contributing users 
via Gitlab-CI. Contribute as explained in [ReadTheDocs](https://ai-agents-for-osm.readthedocs.io/en/latest/) and add tests to 
ensure that your contribution has coverage and then pull the gitlab-ci image from [DockerHub](https://hub.docker.com/r/atosresearch/osm-ai-agent) 
to test your version in a running OSM.

```bash
REGISTRY=https://$NEXUS_USER:$NEXUX_PAT@registry.atosresearch.eu:8443/repository/common-pypi/simple
pip install -e . --extra-index-url https://$NEXUS_USER:$NEXUX_PAT@registry.atosresearch.eu:8443/repository/common-pypi/simple
```

## How to run tests locally

Tests require a running version of the Monitoring data sources deployed in the TelecomTestbed of ARI. Connect to the VPN
and run ```pytest tests```.

## Deploy

Follow the procedure in [how to use](https://ai-agents-for-osm.readthedocs.io/en/latest/how_to_use.html). There is an example of
a NS and VNF descriptor inside the **examples/** folder.

# Contribution Guidelines

This repo follows a Gitflow methodology. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release
2. Pull the latest develop and branch off of it with a branch or MR created from GiLab
3. Commit and push frequently
4. When ready, open a Merge request to develop and wait for approval
5. Only hotfixes and planned releases will be merged into Master and a new docker image will be made available
