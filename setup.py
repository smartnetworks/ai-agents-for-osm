from pathlib import Path

from setuptools import setup

_name = "aiagent"

with open(Path(__file__).parent / Path('README.md')) as readme_file:
    README = readme_file.read()

with open(Path(__file__).parent / Path('requirements.txt')) as req_file:
    INSTALL_REQUIRES = req_file.readlines()

with open(Path(__file__).parent / Path('requirements_docs.txt')) as req_file:
    INSTALL_REQUIRES.extend(req_file.readlines())

setup(name=_name,
      version="1.2.0",
      license="OSI Approved :: Apache Software License",
      description='Artificial Intelligence Agent for proactive scaling of Virtual Network Functions.',
      long_description=README,
      long_description_content_type='text/markdown',
      author='2020, ATOS Research and Innovation',
      author_email='smartnet@atosresearh.eu',
      maintainer='Guillermo Gomez, Luigi Girletti, Paula Encinar',
      url='https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agen',
      install_requires=INSTALL_REQUIRES,
      python_requires='~=3.8',
      packages=['aiagent'],
      include_package_data=True,
      entry_points={
          'console_scripts': [
              'aiagent=aiagent.cli:aiagent',
          ]
      },
      zip_safe=False
)
