import requests

from sns_logger.logger import get_logger

log = get_logger(__name__)


class ModelInterface:
    def __init__(self, model_d):
        """
        Interface for the parameterization, configuration and request to the AI Model server.

        Args:
            model_d (dict): dictionary of the selected model, in which you can find the endpoint of the model you will work with
        """
        self.endpoint = model_d['endpoint']

    def get_health(self):
        """
        Check that the AI Model Server is accessible.
        """
        log.info("Check if AI Model Server is healthy")
        try:
            requests.get(self.endpoint)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
            raise ConnectionError(f"AI Model Server not reachable at {self.endpoint}")
        log.info(f"AI Model Server reachable and up in {self.endpoint}")

    def ai_evaluation(self, model_input):
        """
        AI evaluation Requests the forecast from the AI Server and stores it in the forecast data

        Args:
            model_input (dict): metrics data to feed to AI Model

        Returns (dict): Returns the requested data from the execution of the AI Model
        """
        log.info(f'Preparing model request: {self.endpoint}')
        forecast_data = requests.post(self.endpoint, json=model_input).json()  # TODO: handle invalid response
        log.info(f'model requested: {forecast_data}')
        return forecast_data
