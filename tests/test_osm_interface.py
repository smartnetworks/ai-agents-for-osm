import json
from pathlib import Path
from unittest import mock
import pytest

from aiagent.interfaces.osm_interface import OSMInterface

# Mock-related code that should be ignored
# pylint: disable=E1101


class MongoMock:
    def __init__(self, file):
        self.file = file

    def find(self, *args, **kwargs):
        return self._return_file_content()

    def find_one(self, *args, **kwargs):
        return self._return_file_content()

    def _return_file_content(self):
        with open(Path(__file__).parent / Path('resources', self.file)) as resource:
            payload = resource.read()
        return json.loads(payload)


@mock.patch('aiagent.interfaces.osm_interface.OSMInterface._connect_to_mongo')
@mock.patch('aiagent.interfaces.osm_interface.OSMInterface._mongo_setup')
def test_operational_data(mongo_mock, mock_mongo_setup):
    osm_interface = OSMInterface()
    osm_interface.mongo_client = {
        "vnfrs": MongoMock("vnfrs_example.json"),
        "nsrs": MongoMock("nsrs_example.json"),
        "vnfds": MongoMock("vnfds_example.json"),
    }
    operational_data = osm_interface.get_operational_data('')
    with open(Path(__file__).parent / Path('resources', 'expected_operational_data.json')) as payload:
        expected_operational_data = json.loads(payload.read())
    assert operational_data == expected_operational_data


@mock.patch('aiagent.interfaces.osm_interface.MongoClient')
def test_osm_deployment_cross_compatibility(mongo_client_mock):
    def mock_init(service_name, port):
        from pymongo.errors import ServerSelectionTimeoutError
        if service_name == 'mongodb-k8s':
            raise ServerSelectionTimeoutError
        return {'osm': {'nsrs': MongoMock('nsrs_example.json')}}  # contents not tested here

    mongo_client_mock.side_effect = mock_init
    osm_interface = OSMInterface()
    assert osm_interface.mongo_service_name == 'mongo'


@mock.patch('aiagent.interfaces.osm_interface.MongoClient')
def test_aiagent_fail_with_no_mongo(mongo_client_mock):
    from pymongo.errors import ServerSelectionTimeoutError

    def mock_init(service_name, port):
        raise ServerSelectionTimeoutError

    mongo_client_mock.side_effect = mock_init
    with pytest.raises(ServerSelectionTimeoutError):
        OSMInterface()
