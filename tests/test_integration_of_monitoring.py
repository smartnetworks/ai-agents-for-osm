import pytest

from aiagent.interfaces.monitoring_interface import MonitoringInterface


@pytest.mark.parametrize(
    ["monitoring_d", 'data_name'], (
        [{"endpoint": "http://192.168.137.34:3000"}, 'images'],
        [{"endpoint": "http://192.168.137.34:4000"}, 'normalized hours'],
    )
)
def test_get_metrics_testbed_example(monitoring_d, data_name):
    monitoring_interface = MonitoringInterface(monitoring_d, None)
    input_data = monitoring_interface.get_metrics()
    assert isinstance(input_data, dict)
    assert isinstance(input_data['instances'], list)
    print(f"Source {monitoring_d} provided {data_name}: {input_data['instances'][0]}")
