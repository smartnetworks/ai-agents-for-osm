import json
from pathlib import Path
from unittest import mock
import pytest

from aiagent.interfaces.prometheus_client import PrometheusClient


def test_process_valid_response():
    with open(Path(__file__).parent / Path('resources', 'prometheus_response_single_instance.json')) as resource:
        payload = json.loads(resource.read())
    prom_interface = PrometheusClient()
    prom_interface._request_past_data = mock.Mock(return_value=payload)
    data_d = prom_interface.range_query('mock', 'vnf_i_id')
    assert len(data_d) == 1
    assert all([key in data_d[0].keys() for key in ['values', 'metric']])


@pytest.mark.parametrize(
    ['user_input_url'], (['prometheus'],
                         ['http://prometheus'],
                         ['prometheus/'],
                         ['http://prometheus/'])
)
def test_url_compose(user_input_url):
    prom_client = PrometheusClient(user_input_url, port=9090)
    assert prom_client.prometheus_url == 'http://prometheus:9090/api/v1/query_range'
