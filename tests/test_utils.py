from pathlib import Path
from base64 import b64encode

import pytest
import yaml
import json

import aiagent.utils as utils

def test_validation_success():
    with open(Path(__file__).parent / Path('resources', "config.yaml"),'rb') as resource:
        config = yaml.load(resource.read())

    config_json =json.dumps(config)
    config_b64 = b64encode(config_json.encode('utf-8'))
    success = utils.aiagent_config_validator(config_b64)
    assert success is not None

def test_validation_failure():
    with open(Path(__file__).parent / Path('resources', "config_invalid_model_endpoint.yaml"),'rb') as resource:
        config = yaml.load(resource.read())

    config_json =json.dumps(config)
    config_b64 = b64encode(config_json.encode('utf-8'))
    with pytest.raises(Exception) as e_info:
       utils.aiagent_config_validator(config_b64)


def test_validation_failure_nob64():
    with open(Path(__file__).parent / Path('resources', "config.yaml"), 'rb') as resource:
        config = yaml.load(resource.read())

    config_json = json.dumps(config)
    with pytest.raises(Exception) as e_info:
        utils.aiagent_config_validator(config_json)


def test_url_composer():
    res = utils.url_composer("http://test.com",8080,"app/v1/")
    assert res == "http://test.com:8080/app/v1/"