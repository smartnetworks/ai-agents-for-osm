Scope
*******
`OSM <https://osm.etsi.org>`_ (Open Source MANO) is a well-known `ETSI <https://www.etsi.org>`_-hosted project
providing an open source Management and Orchestration (MANO) stack aligned
with `ETSI NFV <https://www.etsi.org/technologies/nfv>`_ Information Models. This **AI-Agents for
OSM** project is intended for automating the deployment of Artificial Intelligence Agents on the VNFs
orchestrated from the OSM platform. The aim is to be able to provide the Virtual Network Functions
(VNFs) deployed in OSM with artificial intelligence capabilities. The following figure illustrates
the general concept:

.. image:: imgs/Fig-1_Scope.PNG 
    :align: center
    :alt: AI-Agents - General Concept

As we see, individual AI-Agents can be attached to specific VNFs associated to different Network
Services to perform AI related tasks. Of course, using AI-Agents in VNFs is optional, so there can
be VNFs with or without AI-Agent.

VNFs with AI-Agents attached will have additional AI/ML capabilities associated to the Service Assurance (SA) set of actions already available in OSM, namely:
 1. Alerting actions (VNF metrics can trigger threshold-violation alarms), and 
 2. Auto-scaling actions (automatically increase or reduce the number of VNF instances). 

Without **AI-Agents for OSM** these actions are typically executed based on the definition of simple
rules or thresholds in OSM, associated to certain by-default metrics (CPU usage, memory usage...),
but using **AI-Agents for OSM** this behavior can be enriched, enabling the possibility of triggering those
alerting or scaling actions based also on more complex metrics leveraging AI/ML algorithms.

In fact, the **AI-Agents for OSM** functionality can be used to enhance the VNFs deployed on OSM, allowing (for instance) to define *proactive*
alerting or scaling behaviors based on forecasting certain service usage patterns; also, to trigger
scaling actions based on image recognition or data clustering algorithms, among other
AI/ML-based applications.

In summary, **AI-Agents for OSM** allows to go beyond the purely *reactive* OSM behaviour and the
limited set of metrics defined on it, allowing the integration of AI/ML capabilities so expanding
the by-default OSM functionality.


The deployment of the **AI-Agents for OSM** functionality is based on the Execution Environments (EE) feature
introduced in `OSM Release EIGHT <https://osm.etsi.org/wikipub/images/5/56/OSM_Release_EIGHT_-_Release_Notes.pdf>`_.
If you want to know more details about the internal details and the implementation you can go to the
next :ref:`Architecture Overview <target to architecture>` section. If
you want to go straight forward to deploy a simple 'hello world' example you can jump to
the :ref:`Usage <target to usage>` section (you will find an example to trigger
scaling actions based on image recognition algorithms).

