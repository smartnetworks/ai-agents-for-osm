import os
import time

from sns_logger import logger

from aiagent import utils
from aiagent.interfaces.monitoring_interface import MonitoringInterface
from aiagent.interfaces.osm_interface import OSMInterface
from aiagent.interfaces.model_interface import ModelInterface
from aiagent.interfaces.threshold_interface import ThresholdInterface

log = logger.get_logger(__name__)


class AIAgent:
    def __init__(self):
        """
        Orchestrator that executes proactive scaling for a VNF in a scheduled job running inside the EE of OSM
        """
        self.osm_interface = OSMInterface()
        self.monitoring_interface = None
        self.model_interface = None
        self.threshold_interface = None
        self.config = None

    def _initialize(self):
        """
        environ coming from cronjob.yaml
        e.g
        containers:
              - image: "{{ $job.image.repository }}:{{ $job.image.tag }}"
                imagePullPolicy: {{ $job.image.imagePullPolicy }}
                name: {{ $job.name }}
                env:
                - name: config
                  value: {{$job.config | toJson | b64enc | quote }}
                - name: vnf-id
                  value: "{{ $vnf_id | lower}}"
        """
        # Execute this on every cycle so we can pick up updates from config files
        config = os.environ.get('config', '{}')
        self.vnf_id = os.environ.get('vnf-id')
        token = os.environ.get('NBI-Token')
        self.config = utils.aiagent_config_validator(config)
        self.osm_interface.nbi_token = token

    def run(self):
        """
        Main method that executes a single sequence of the AIAgent workflow.
        Extract monitoring metrics --> feed metrics to AI Model --> evaluate AI Model output with threshold
        """
        self._initialize()
        operational_data = self.osm_interface.get_operational_data(self.vnf_id)
        log.info(f'OSM Data to run AI model evaluation: {operational_data}')
        # TODO: Check if there is still a previous Agent running
        for execution in self.config['executions']:
            if not execution['active']:
                log.info(f"Not Running {execution} because is NOT active")
                continue
            log.info(f"Running {execution} because is active")
            self.model_interface = ModelInterface(execution['model'])
            log.info(f"AI URL : {self.model_interface.endpoint}")
            self.model_interface.get_health()
            self.monitoring_interface = MonitoringInterface(execution['monitoring'], operational_data)
            self.threshold_interface = ThresholdInterface(execution['threshold'])
            input_data = self.monitoring_interface.get_metrics()
            ai_forecast_data = self.model_interface.ai_evaluation(input_data)
            ai_result = self.threshold_interface.ai_threshold(ai_forecast_data)
            self.request_scale_action(operational_data, ai_result)

    def request_scale_action(self, operational_data, ai_result):
        """
        Final action that the AIAgent triggers. Currently supporting Scaling action

        Args:
            ai_result (bool): Result from the evaluation of the threshold with the output of the AI Model
            operational_data (dict): Relevant information obtained from OSM
        """
        scale_kwargs = {
            'nsi_id': operational_data['nsi_id'],
            'project_id': operational_data['project_id'],
            'scaling_group': operational_data['scaling-group-descriptor'],
            'vnf_index': operational_data['member-vnf-index-ref']
        }
        if ai_result and len(operational_data['vdu-data']) < operational_data['max-instances']:
            log.info("SCALING OUT")
            scale_kwargs.update({'scale': 'SCALE_OUT'})
        elif not ai_result and len(operational_data['vdu-data']) > operational_data['min-instances']:
            log.info("SCALING IN")
            scale_kwargs.update({'scale': 'SCALE_IN'})
        else:
            log.info("No actions required")
            return False
        log.info(f"Scale kwargs {scale_kwargs}")
        operation_id = self.osm_interface.scale_ns(**scale_kwargs)['id']
        start_time = time.time()
        elapsed_time = time.time() - start_time
        while elapsed_time <= 60:  # max 30 seconds waiting
            log.info(f"Loop waiting scale to finished {elapsed_time} (MAX 30 secs)")
            elapsed_time = time.time() - start_time
            operation_info_d = self.osm_interface.get_operation_state_scale(operational_data['project_id'],
                                                                            operation_id)
            log.info(operation_info_d)
            if operation_info_d['operationState'] == "COMPLETED":
                break
            elif operation_info_d['operationState'] == "FAILED":
                log.info(operation_info_d['errorMessage'])
                raise ValueError(operation_info_d['errorMessage'])
            elif operation_info_d['operationState'] != "COMPLETED" and elapsed_time >= 30:
                log.info("Scaling has not been completed")
                raise ValueError("Scaling has not been completed")
            else: 
                log.info("Looping to wait for action to finish")
            time.sleep(5)
        return True
