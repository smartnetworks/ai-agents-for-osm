from click.testing import CliRunner
from unittest import mock

from aiagent import cli


# Mock-related code that should be ignored
# pylint: disable=E1101

def test_start_commands_available():
    runner = CliRunner()
    result = runner.invoke(cli.aiagent, ['start', '--help'])
    assert result.exit_code == 0


@mock.patch('aiagent.agent.AIAgent.run')
@mock.patch('aiagent.agent.OSMInterface._mongo_setup')
def test_ai_agent_start(mock_agent_run, mock_osm_mongo):
    runner = CliRunner()
    result = runner.invoke(cli.aiagent, ['start'])
    assert result.exit_code == 0
