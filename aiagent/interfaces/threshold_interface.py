from pathlib import Path
from importlib import import_module

from sns_logger.logger import get_logger

log = get_logger(__name__)


class ThresholdInterface:
    def __init__(self, threshold_d):
        """
        Class in charge of evaluating the AI Result from a AI Model using a user defined threshold.

        Args:
            threshold_d (dict): Configuration dict extracted from the AI Agent values.yml
        """
        self.logic = threshold_d['logic']
        self.function_name = threshold_d['function_name']

    def ai_threshold(self, forecast_data):
        """
        AI Forecast evaluates Forecast Data by the threshold defined in helm charm

        Args:
            forecast_data (dict): data of the chosen model

        Returns (bool): Evaluation of the threshold defined
        """
        # TODO: Research a more direct approach
        file_path = Path.cwd() / Path('threshold_evaluator_function.py')
        if not file_path.exists():
            file_path.touch()
        with open(file_path, 'w') as out_file:
            out_file.write(self.logic)
        log.info(file_path.read_text())
        evaluation_function = getattr(import_module('threshold_evaluator_function'), self.function_name)
        log.info(f"Evaluating threshold: {evaluation_function(forecast_data)}")
        result = evaluation_function(forecast_data)
        file_path.unlink()
        return result
