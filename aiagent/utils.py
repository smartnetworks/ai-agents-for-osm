import json
from base64 import b64decode
from pathlib import Path
import jsonschema as js

from sns_logger import logger

log = logger.get_logger(__name__)


def url_composer(url, port=None, endpoint=None):
    """
    URL composer

    Args:
        url (str): URL to compose
        port (int): optional port to compose
        app (str): optional web application path
    Returns:
        composed URL
    """
    port = f":{port}" if port else ""
    endpoint = f"/{endpoint}" if endpoint else ""
    url = f"http://{url.replace('http://', '').rstrip('/')}{port}{endpoint}"
    return url


def aiagent_config_validator(config):
    """
    Config Validator

    Args:
        config (dict): configuration of the AI Agent taken from the values.yaml
    Returns:
        The AI Agent configurations validated, otherwise raises an exception if the validation fails.

    """

    try:
        with open(Path(__file__).parent / Path("config_schema.json")) as resource:
            schema = json.loads(resource.read())
        values_json = json.loads(b64decode(config))
        log.info(f'Checking Config:\n{values_json}')

        js.validate(instance=values_json, schema=schema)
    except Exception as e:
        log.error(f"Failed to validate values.yml provided. Check config_schema.json and the documentation.")
        log.error(e, exc_info=True)
        raise e
    else:
        return values_json
