Q&A
====

Which OSM release do we need?
*****************************

The AI Agents for OSM functionality has been tested for the releases from OSM NINE onwards. Previous versions are not
compatible due to a breaking change in the Information Model of the VNF descriptors.

Could we deploy these AI Agents on other ETSI NFV platforms besides OSM?
*************************************************************************

The current configuration of the OSM AI Agent requires a running OSM and cannot be used in a different NFV platform
without changes in the source code.

Can we use an AI framework other than Tensor Flow Serving?
***********************************************************

Yes. Tensor Flow Serving has been refered here just for demonstration purpouses, but other AI frameworks (both, open-source or propietary) could be used as well. AI Agents only expect an endpoint for a REST API to interface with the AI Models Server, so this is the only limitation to interface with the AI Models in the AI Models Server. You can configure the AI Model Server in use in the 'values.yaml' file (see 'Configuration' in Section 3). There you can even define different AI Models Servers by using multiple AI Evaluations.

Is it possible to perform other orchestration actions beyond scaling?
**********************************************************************

The *AI Agents for OSM* functionality is limited by the offered orchestration actions available in OSM. At this moment (OSM Rel. NINE) only alerting and
scaling actions are available. 

Which VIMs can be used to host the VNFs created by running the descriptors from OSM to launch AI-Agents?
*************************************************************************************************************

All those allowed by OSM. You can see the supported VIMs here: https://osm.etsi.org/docs/user-guide/04-vim-setup.html?highlight=vim

Which AI Models are supported?
******************************

There is no a explicit limitation on the supported AI Models. For example, you could use different types of ML algorithms or neural network topologies at your discretion to adress the specific problem you want to solve in the best way you may consider. However, there is a limitation regarding the learning models: the current functionality is just intended for learning models where training and production stages are clearly separated (for instance, supervised or unsupervised learning models). Cases that may require continuous fitting of the model (e.g. reinforcement learning models) are currently out of scope, as they would require re-definition of the interface between the AI-Agent and the AI Models Server.

Can the conditions to be assessed by the thresholds be modified?
****************************************************************

Yes, the threshold conditions can be modified according to the needs of the AI Model and the project to obtain the desired results (this can be done by modifying the *values.yaml* file - see 'Configuration' in Section 3).

If I have several models in the 'values.yaml' file, how can I select the one I want to work with?
*************************************************************************************************

In this file you can add all the models that you consider necessary or that you are going to use. To select a model you just need to act on the boolean *active* field; toggling this field to true/false you can activate/deactivate the corresponding model.

Is it possible to have several AI Agents running?
*************************************************

Yes, it is totally possible to have several agents running (in fact this is the purpouse). However, of course, the overall performance can be affected depending on the number of deployed agents, so it is advisable to remove those agents that are not being used.

What OSM installation options should we use?
********************************************

For using this *AI Agents for OSM functionality* it is mandatory to perform a Kubernetes based installation. However, in the most recent versions at the time of writing this document (OSM NINE and TEN) the Kubernetes installation is the default option, so in these cases it is enough to perform a default installation without selecting any other installation options.

Is this an *official* OSM feature?
**********************************

The OSM Community considers that *Official OSM features* are just those impacting on the internal OSM architectural components (MON, POL, PLA, etc.). According to this, since AI Agents are attached to VNFs  through their Execution Environment, this *AI Agents for OSM* functionality is not considered an *official* OSM feature itself. However, this functionality has been defined in a close interaction with the ETSI OSM Community. A PoC showcasing the concept described here can be found in the official ETSI OSM PoCs repository (https://osm.etsi.org/wikipub/index.php/OSM_PoCs).

