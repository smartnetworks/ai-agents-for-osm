Annex 2. AI Agent Code
#######################

The source code of this project is available in the `ATOS GitLab <https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent>`_ repository.
Examining the code you can see that AI Agents are built using the same approach as the configuration file used to run them. There is an 
orchestrator and a set of interfaces that implement each section of the configuration file.

.. code-block:: yaml

    jobs:
      # Orchestration configuration
      - name: atos-ai-agent
        image:
          repository: atosresearch/osm-ai-agent
          tag: latest
          imagePullPolicy: Always
        schedule: "* * * * *"
        failedJobsHistoryLimit: 2
        successfulJobsHistoryLimit: 2
        concurrencyPolicy: Allow
        restartPolicy: OnFailure
        config:
          executions:
            - active: True
              # AIAgents interfaces
              # AI Model Configuration
              model:
                endpoint: "http://192.168.137.46:8501/v1/models/CPU-forecast-model:predict"
              # Monitoring Service Configuration
              monitoring:
                endpoint: "http://192.168.137.34:4000"
              # Threshold evaluation Configuration
              threshold:
                function_name: evaluator
                logic: "evaluator = lambda x: True if x['predictions'][0][0] >= 0.8 else False"

AI Agent Orchestrator
=====================

.. autoclass:: aiagent.agent.AIAgent
    :members:


Interfaces
===========


Model Interface
--------------------


.. autoclass:: aiagent.interfaces.model_interface.ModelInterface
    :members:


Monitoring Interface
---------------------


.. autoclass:: aiagent.interfaces.monitoring_interface.MonitoringInterface
    :members:


Threshold Interface
--------------------


.. autoclass:: aiagent.interfaces.threshold_interface.ThresholdInterface
    :members:
