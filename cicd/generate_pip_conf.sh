#/bin/bash

NEXUS=$1

cat > pip.conf << EOF
[global]
index-url = https://pypi.python.org/simple
timeout=60
extra-index-url = $NEXUS
EOF