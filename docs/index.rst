Welcome to AI Agents for OSM's documentation!
==============================================
|version| |coverage| |pylint|

.. toctree::
   :maxdepth: 2
   :numbered: 2
   :caption: Contents:

   scope
   architecture
   how_to_use
   how_to_contribute
   q_and_a
   roadmap
   license
   contact
   acknowledgement
   Annex1
   code

`OSM (Open Source MANO) <https://osm.etsi.org>`_ is a well-known `ETSI <https://www.etsi.org>`_-hosted project providing an open source Management and Orchestration (MANO) 
stack aligned with the `ETSI NFV <https://www.etsi.org/technologies/nfv/>`_ Information Models. The **AI-Agents for OSM** functionality described here is intended for 
automating the deployment of Artificial Intelligence Agents on the Virtual Network Functions (VNFs) orchestrated from the OSM platform. This functionality has been developed
in the in the context of the `H2020 5GTours Project <https://5gtours.eu/>`_ and in a close communication with the Open Source MANO community. The code is OpenSource, and it 
is available from the `ATOS GitLab <https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent>`_ repository.


.. |coverage| image:: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Coverage.svg?job=make_badges
    :alt: Test coverage
    :target: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent


.. |pylint| image:: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Pylint.svg?job=make_badges
    :alt: Pylint result
    :target: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent

.. |version| image:: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent/-/jobs/artifacts/develop/raw/Version.svg?job=make_badges
    :alt: Version
    :target: https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent
