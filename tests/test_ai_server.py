import pytest
import json
from pathlib import Path

from sns_logger.logger import get_logger

from aiagent.interfaces.model_interface import ModelInterface

log = get_logger(__name__)


@pytest.mark.parametrize(
    ['monitoring_input', 'is_jam'],
    (
            ['image_no_traffic.json', False],
            ['image_with_traffic.json', True],
    )
)
def test_traffic_model(monitoring_input, is_jam):
    with open(Path(__file__).parent / Path('resources', monitoring_input)) as resource:
        payload = json.loads(resource.read())
    model_d = {"endpoint": "http://192.168.137.46:8501/v1/models/A2-traffic-model:predict" }
    model_interface = ModelInterface(model_d)
    result = model_interface.ai_evaluation(payload)
    assert isinstance(result, dict)
    if is_jam:
        assert result['predictions'][0][0] >= result['predictions'][0][1]
    else:
        assert result['predictions'][0][0] <= result['predictions'][0][1]


def test_hour_model():
    with open(Path(__file__).parent / Path('resources', 'hour_no_scale.json')) as resource:
        payload = json.loads(resource.read())
    model_d = {"endpoint": "http://192.168.137.46:8501/v1/models/CPU-forecast-model:predict"}
    model_interface = ModelInterface(model_d)
    result = model_interface.ai_evaluation(payload)
    log.info(f'result: {result}')
    assert isinstance(result, dict)
    assert result['predictions'][0][0] < 0.5