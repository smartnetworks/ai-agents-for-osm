Contact
********

The *AI Agents for OSM* project is an Open Source Project maintained by the Smart Networks 
and Services Unit of the `ATOS <https://atos.net/en>`_ `Research and Innovation (ARI) <https://booklet.atosresearch.eu>`_ department (check the `source code <https://scm.atosresearch.eu/ari/pub-osm/pub-osm-ai-agent>`_ or contact via `email <smartnetworks@atosresearch.eu>`_).

For any bug, feature request or technical support please refer to :ref:`How to Contribute <target to report_a_bug>`.

.. image:: imgs/aiagents-logo.PNG
    :align: center
    :alt: AI Agents logo for OSM
