import datetime

import requests
import json
import urllib3
from uuid import uuid4
from pymongo import MongoClient, DESCENDING
from pymongo.errors import ServerSelectionTimeoutError

from sns_logger import logger

log = logger.get_logger(__name__)

# Disable the InsecureRequestWarning for the requests to OSM
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class OSMInterface:
    # TODO: Use Mongo only to get the project-id and tokens. Everything else take it from NBI.
    # TODO: Use kafka to raise actions
    def __init__(self, token=None):
        self.nbi_token = token
        self.mongo_service_name = ''
        self.mongo_client = None
        self._mongo_setup()
        self.base_url = "https://nbi:9999/osm"
        self.operation_state_url = f"{self.base_url}/nslcm/v1/ns_lcm_op_occs"
        self.scale_ns_url = f"{self.base_url}/nslcm/v1/ns_instances"

    @property
    def nbi_token(self):
        return self._nbi_token

    @nbi_token.setter
    def nbi_token(self, token):
        self._nbi_token = str(token) or str(uuid4())

    def _mongo_setup(self):
        self._find_mongo_service_name()
        self._connect_to_mongo()

    def _connect_to_mongo(self):
        self.mongo_client = MongoClient(self.mongo_service_name, 27017)['osm']

    def _find_mongo_service_name(self):
        try:
            self.mongo_service_name = 'mongodb-k8s'
            self._connect_to_mongo()
            self.mongo_client['nsrs'].find()
        except ServerSelectionTimeoutError as service_name_exception:
            log.info("Mongo was not found in k8s service names 'mongodb-k8s")
            log.debug(service_name_exception, exc_info=True)
            try:
                self.mongo_service_name = 'mongo'
                self._connect_to_mongo()
                self.mongo_client['nsrs'].find()
            except ServerSelectionTimeoutError as service_name_exception:
                log.error("Mongo Db was not found in any of the known services names [mongo, mongodb-k8s]. "
                          "AIAgent will fail")
                raise service_name_exception

    def get_operational_data(self, vnf_id):
        """
        Obtains the operational data required to run a model execution. It is currently using MongoDB as datasource

        Args:
            vnf_id (str): VNF instance ID obtained via EE vars

        Returns (dict):
             Operational Data required for the AIAgent to work
                    {
                        'member-vnf-index-ref': <string>,
                        'nsi_id': <string>,
                        'vdu-data': <list>,
                        'ns_name': <string>,
                        'vnfs': <list>,
                        'project_id': <string>,
                        'scaling-group-descriptor': <string>
                    }
        """
        self._connect_to_mongo()
        vnfi_data = self.mongo_client['vnfrs'].find_one({'_id': vnf_id})
        log.info(f"VNFi Data: {vnfi_data}")
        if vnfi_data['_admin']['nsState'] != 'INSTANTIATED':
            raise ValueError("NS not fully ready yet")
        log.info("#########################################################################")
        nsi_id = vnfi_data['nsr-id-ref']
        ns_data = self.mongo_client['nsrs'].find_one({'_id': nsi_id})
        log.info(f"NS data: {ns_data}")
        vnfd_data = self.mongo_client['vnfds'].find_one({'_id': vnfi_data['vnfd-id']})
        log.info("#########################################################################")
        log.info(f"VNFd Data: {vnfd_data}")
        if not vnfd_data['df'][0].get('scaling-aspect'):
            log.debug(f"vnfrs: {vnfi_data}")
            log.debug(f"vnfds: {vnfi_data}")
            raise ValueError("Unable to find scaling-group-descriptor ref for the NBI (scaling-aspect)")
        scaling_group = vnfd_data['df'][0]['scaling-aspect'][0]['name']
        # TODO: OSM has a bug that deletes max-number-of-instances
        vdu_profile_max = vnfd_data['df'][0]['vdu-profile'][0].get('max-number-of-instances', 2)
        vdu_profile_min = vnfd_data['df'][0]['vdu-profile'][0].get('min-number-of-instances', 1)

        operational_data = {
            'member-vnf-index-ref': vnfi_data['member-vnf-index-ref'],
            'nsi_id': nsi_id,
            'vdu-data': [
                {'vdu-id-ref': vdu.get('vdu-id-ref'), 'ip-address': vdu.get('ip-address'), 'name': vdu.get('name')}
                for vdu in vnfi_data.get('vdur')
            ],
            'ns_name': ns_data.get('name'),
            'vnfs': ns_data.get('constituent-vnfr-ref', []),
            'project_id': ns_data.get('_admin').get('projects_write', [None])[0],
            'scaling-group-descriptor': scaling_group,  # TODO: Update to scaling-group when OSM fixes its IM
            'max-instances': vdu_profile_max,
            'min-instances': vdu_profile_min
        }

        log.info(f"VNFi info extracted from OSM: {operational_data}")
        return operational_data

    def get_operation_state_scale(self, project_id, action_id):
        """
        Checks the status of the escalation operation, to know through its id whether the operation has been completed, continues to scale or has failed.

        Args:
            project_id (str): OSM Project identification
            action_id (str): Scale operation action id in OSM 

        Returns (dict): operation status
        """
        self._connect_to_mongo()
        token = self.update_token(project_id)
        headers = {'Authorization': token, 'accept': 'application/json'}
        url = f"{self.operation_state_url}/{action_id}"
        response = requests.get(url, params=None, verify=False, stream=True, headers=headers)
        response.raise_for_status()
        return response.json()

    def scale_ns(self, nsi_id, project_id, scaling_group, vnf_index, scale="SCALE_OUT"):
        """
        Scale in/out trigger for NS.

        Args:
            nsi_id (str): Network Service instance identification
            project_id (str): OSM Project identification
            scaling_group (str): Scaling group action (Specified  in the VNF descriptor)
            vnf_index (str): Virtual Network Function index (Specified  in the VNF descriptor)
            scale (str): Scaling action 2 possibilities: 'SCALE_OUT' Or 'SCALE_IN'

        Returns (dict):
            Scaling action response
        """
        self._connect_to_mongo()
        token = self.update_token(project_id)
        headers = {'Authorization': token, 'accept': 'application/json'}
        url = f"{self.scale_ns_url}/{nsi_id}/scale"
        scale_data = {
            "scaleType": "SCALE_VNF",
            "timeout_ns_scale": 1,
            "scaleVnfData": {
                "scaleVnfType": scale,
                "scaleByStepData": {
                    "scaling-group-descriptor": scaling_group,
                    "scaling-policy": "string",
                    "member-vnf-index": vnf_index
                }
            }
        }
        response = requests.post(url, data=json.dumps(scale_data), verify=False, headers=headers)
        response.raise_for_status()
        log.info(f"Scale action request response : {response.json()}")
        self.delete_token()
        return response.json()

    def update_token(self, project_id):
        """
        NBI Token generator to provide authorization access

        Args:
            project_id (str): OSM Project identification

        Returns (str):
            token to perform requests in an authorized way
        """
        date = datetime.datetime.utcnow().timestamp()
        token_data = self.mongo_client['tokens'].find_one({'project_id': project_id}, sort=[('expires', DESCENDING)])
        if not token_data:
            token_data = {}
        token_data['_id'] = self.nbi_token
        token_data['id'] = self.nbi_token
        token_data['issued_at'] = date
        token_data['expires'] = date + 5

        try:
            self.mongo_client['tokens'].delete_one({'id': self.nbi_token})
        except Exception as e:
            log.debug(e)

        self.mongo_client['tokens'].insert_one(token_data)
        token = f"Bearer {self.nbi_token}"
        return token

    def delete_token(self):
        """
        Remove used token for scaling actions to be deleted
        """
        self.mongo_client['tokens'].delete_one({'id': self.nbi_token})
