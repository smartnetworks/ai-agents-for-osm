import json
from pathlib import Path

from sns_logger import logger

from aiagent.interfaces.threshold_interface import ThresholdInterface

log = logger.get_logger(__name__)


def test_threshold():
    with open(Path(__file__).parent / Path('resources', 'prediction.json')) as resource:
        payload = json.loads(resource.read())
    threshold_d = {"function_name": "evaluator", "logic": "evaluator = lambda x: False if (x['predictions'][0][0] < x['predictions'][0][1]) else True "}
    threshold_interface = ThresholdInterface(threshold_d)
    result = threshold_interface.ai_threshold(payload)
    log.info(result)
    assert not result
